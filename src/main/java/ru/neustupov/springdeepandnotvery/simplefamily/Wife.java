package ru.neustupov.springdeepandnotvery.simplefamily;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Wife {

  @Autowired
  private Husband husband;

  @PostConstruct
  public void init() {
    husband.doSomething();
    System.out.println("Жена: Мне нечего одеть!");
  }

  public void saySomething() {
    System.out.println("Жена: куда ты собрался?");
  }
}
