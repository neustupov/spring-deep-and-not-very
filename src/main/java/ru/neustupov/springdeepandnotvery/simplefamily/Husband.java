package ru.neustupov.springdeepandnotvery.simplefamily;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Husband {

  @Autowired
  private Wife wife;

  @PostConstruct
  public void init() {
    wife.saySomething();
    System.out.println("Муж: сегодня футбол!");
  }

  public void doSomething() {
    System.out.println("Муж: я пожалуй на рыбалку");
  }
}
