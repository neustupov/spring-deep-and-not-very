package ru.neustupov.springdeepandnotvery.baruchandjeka;

import java.util.List;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JekaService {

  @Autowired
  @Getter
  @Jeka
  private List<String> list;
}
