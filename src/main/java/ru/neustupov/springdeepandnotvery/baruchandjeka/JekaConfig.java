package ru.neustupov.springdeepandnotvery.baruchandjeka;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Bean;

public class JekaConfig {

  @Bean
  @Jeka
  public List<String> messages() {
    List<String> strings = new ArrayList<>();
    strings.add("str3");
    strings.add("str4");
    return strings;
  }
}
