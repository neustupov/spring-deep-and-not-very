package ru.neustupov.springdeepandnotvery.baruchandjeka;

import java.util.List;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Demo {

  public static void main(String[] args) {
    AnnotationConfigApplicationContext configApplicationContext = new AnnotationConfigApplicationContext(
        "ru.neustupov.springdeepandnotvery.baruchandjeka");
    List<String> list1 = configApplicationContext.getBean(BaruchService.class).getList();
    System.out.println("Baruch");
    for (String s : list1) {
      System.out.println(s);
    }
    List<String> list2 = configApplicationContext.getBean(JekaService.class).getList();
    System.out.println("Jeka");
    for (String s : list2) {
      System.out.println(s);
    }
  }
}
