package ru.neustupov.springdeepandnotvery.baruchandjeka;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BaruchConfig {

  @Bean
  @Baruch
  public String str1() {
    return "str1";
  }

  @Bean
  @Baruch
  public String str2() {
    return "str2";
  }

  @Bean
  @Baruch
  public String str3() {
    return "XXX";
  }
}
