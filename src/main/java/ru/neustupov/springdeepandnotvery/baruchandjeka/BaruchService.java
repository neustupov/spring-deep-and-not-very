package ru.neustupov.springdeepandnotvery.baruchandjeka;

import java.util.List;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BaruchService {

  @Autowired
  @Getter
  @Baruch
  private List<String> list;
}
