package ru.neustupov.springdeepandnotvery.manyfamily;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Demo {

  public static void main(String[] args) {
    new AnnotationConfigApplicationContext(
        "ru.neustupov.springdeepandnotvery.manyfamily");
  }
}
