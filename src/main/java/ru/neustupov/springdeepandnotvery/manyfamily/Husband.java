package ru.neustupov.springdeepandnotvery.manyfamily;

import javax.annotation.PostConstruct;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Husband {

  @Autowired
  private Wife wife;

  @Getter
  private String money;

  @Main
  public void action() {
    init();
    System.out.println(wife.getCaress().toUpperCase() + " it`s first, " + money + " it`s second");
  }

  @PostConstruct
  private void init() {
    money = "5000$";
  }
}
