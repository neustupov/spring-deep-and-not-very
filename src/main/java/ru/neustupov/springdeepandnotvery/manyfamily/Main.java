package ru.neustupov.springdeepandnotvery.manyfamily;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Main {

}
