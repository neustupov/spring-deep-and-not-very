package ru.neustupov.springdeepandnotvery.manyfamily;

import javax.annotation.PostConstruct;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Wife {

  @Autowired
  private Husband husband;

  @Getter
  private String caress;

  @Main
  public void action() {
    init();
    System.out.println(husband.getMoney().toUpperCase() + " shit, make " + caress);
  }

  @PostConstruct
  private void init() {
    caress = "Love";
  }
}
