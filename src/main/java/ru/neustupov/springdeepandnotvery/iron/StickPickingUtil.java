package ru.neustupov.springdeepandnotvery.iron;

import java.lang.reflect.Field;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.core.type.classreading.MethodMetadataReadingVisitor;

public class StickPickingUtil {

  public static String resolveClassNameForJavaConfig(BeanDefinition beanDefinition) {
    try {
      Object reader = Class.forName(
          "org.springframework.context.annotation.ConfigurationClassBeanDefinitionReader$ConfigurationClassBeanDefinition")
          .cast(beanDefinition);
      Field field = reader.getClass().getDeclaredField("factoryMethodMetadata");
      field.setAccessible(true);
      MethodMetadataReadingVisitor visitor = (MethodMetadataReadingVisitor) field.get(reader);
      return visitor.getReturnTypeName();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

}
