package ru.neustupov.springdeepandnotvery.iron;

public class CopperForPain implements Copper {

  @Override
  public void warmingUp() {
    System.out.println("Copper for pain");
  }
}
