package ru.neustupov.springdeepandnotvery.iron;

import org.springframework.stereotype.Component;

@Component
public class Water {

  @Override
  public String toString() {
    return "Water";
  }
}
