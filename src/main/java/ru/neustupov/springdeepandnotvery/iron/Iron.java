package ru.neustupov.springdeepandnotvery.iron;

import javax.annotation.PostConstruct;

public interface Iron {

  @PostConstruct
  void warmingUp();
}
