package ru.neustupov.springdeepandnotvery.iron;

import java.lang.reflect.Method;
import javax.annotation.PostConstruct;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;

@Component
public class InitMethodRegistryBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

  @Override
  public void postProcessBeanFactory(
      ConfigurableListableBeanFactory factory) throws BeansException {
    String[] names = factory.getBeanDefinitionNames();
    for (String name : names) {
      AbstractBeanDefinition beanDefinition = (AbstractBeanDefinition) factory
          .getBeanDefinition(name);
      String beanClassName = beanDefinition.getBeanClassName();
      if (beanClassName == null) {
        beanClassName = StickPickingUtil.resolveClassNameForJavaConfig(beanDefinition);
      }
      Class<?> bean = ClassUtils
          .resolveClassName(beanClassName, ClassLoader.getSystemClassLoader());
      Class<?>[] allInterfaces = ClassUtils.getAllInterfacesForClass(bean);
      for (Class<?> itr : allInterfaces) {
        Method[] methods = itr.getMethods();
        for (Method method : methods) {
          if (method.isAnnotationPresent(PostConstruct.class)) {
            beanDefinition.setInitMethodName(method.getName());
          }
        }
      }
    }
  }
}
