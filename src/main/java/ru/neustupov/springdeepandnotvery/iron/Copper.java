package ru.neustupov.springdeepandnotvery.iron;

import javax.annotation.PostConstruct;

public interface Copper {

  @PostConstruct
  void warmingUp();
}
