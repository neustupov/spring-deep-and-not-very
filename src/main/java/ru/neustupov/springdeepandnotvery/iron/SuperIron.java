package ru.neustupov.springdeepandnotvery.iron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SuperIron implements Iron {

  @Override
  public void warmingUp() {
    System.out.println("Warming Up");
  }

  @Autowired
  public void warmingWater(Water water) {
    System.out.println(water + " warming");
  }
}
