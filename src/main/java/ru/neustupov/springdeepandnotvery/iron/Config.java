package ru.neustupov.springdeepandnotvery.iron;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

  @Bean
  public Copper copper() {
    return new CopperForPain();
  }
}
