package ru.neustupov.springdeepandnotvery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDeepAndNotVeryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDeepAndNotVeryApplication.class, args);
	}

}

